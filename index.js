'use strict'

const mongoose = require('mongoose');
const app = require('./app');
const port = process.env.PORT || 3000;

/**
* Server config
*/
mongoose.connect('mongodb://localhost:27017/shop', (err, response) => {
  if (err) {
    return console.log('Connection error');
  }
  app.listen(port, () => {
    console.log(`Successful connection. Server on port ${port}`);
  });
});



