'use strict'

const Product = require('../models/product');

function getProducts(req, response) {
  Product.find({}, (err, products) => {
    if (err) return response.status(500).send({ message: 'Server error. Reuquest error' });
    if (!products) return response.status(404).send({ message: `Server error. Product ${product} don´t exist` });
    response.status(200).send({ products });
  });
}

function getProduct(req, response) {
  let productId = req.params.productId;

  Product.findById(productId, (err, product) => {
    if (err) return response.status(500).send({ message: 'Server error. Reuquest error' });
    if (!product) return response.status(404).send({ message: `Server error. Product ${id} don´t exist` });

    response.status(200).send({ product });
  })
}

function setProduct(req, response) {
  let product = new Product();
  product.name = req.body.name;
  product.picture = req.body.picture;
  product.price = req.body.price;
  product.description = req.body.description;
  product.category = req.body.category;

  product.save((err, productStored) => {
    if (err) response.status(500).send('Error on save product on BBDD');
    response.status(200).send({ product: productStored });
  });
}


module.exports = {
  getProducts,
  getProduct,
  setProduct
}