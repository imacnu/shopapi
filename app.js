const express = require('express');
const bodyParser = require('body-parser');

const Product = require('./models/product');
const app = express();

const ProductCtrl = require('./controllers/product');

/**
* Body Parser config
*/
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

/**
* Routes
*/
app.get('/api/products', ProductCtrl.getProducts);
app.get('/api/products/:productId', ProductCtrl.getProduct);
app.post('/api/product', ProductCtrl.setProduct);

module.exports = app;