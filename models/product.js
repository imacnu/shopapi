'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = Schema({
  name: String,
  picture: String,
  price: {
    type: Number,
    default: 0
  },
  description: String,
  category: {
    type: String,
    enum: ['main', 'special']
  }
});


module.exports = mongoose.model('Product', productSchema);